function [out] = unique_counts(input,key)

u = unique(input);
out = nan(length(key),1);

for ii = 1:length(key)
    if isint(key(ii))
        v = input ==key(ii);
        out(ii) = sum(v);
    elseif ischar(key(ii))
        v = strcmp(u, key(ii));
        out(ii) = sum(v);
    end
end      

end