function [ out ] = norm01 ( data , dim , varargin)

% normalizes "data" on a scale from 0 to 1 along the dimension "dim" such
% that 1 normalizes across rows, and 2 is columns, 1 is default
% "data" can be a vector or matrix

if ~exist('dim','var')
    dim = 1;
end

out = data;

for i = 1:size(data,dim)
    if dim == 1;
        x=data(i,:);
        out(i,:) = (x-nanmin(x))/(nanmax(x)-nanmin(x));
    elseif dim == 2;
        x=data(:,i);
        out(:,i) = (x-nanmin(x))/(nanmax(x)-nanmin(x));
    end
end

end