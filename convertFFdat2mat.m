fol = 'f:\desktop\Data\ICls\Dec8-2014\';
cd(fol)
file = '021-2014-1208-07-FF';

[adata, ~, ~, ~] = autoprocess_rf_data([file '.dat']);
reps = length(adata{1}.spiketimes);

TIMES = cell(length(adata),reps);

for i = 1:length(adata)
    TIMES(i,:) = adata{i}.spiketimes;
end

COUNT = cell2mat(cellfun(@length, TIMES, 'UniformOutput', 0));

save([file '.mat'],'COUNT','TIMES')

