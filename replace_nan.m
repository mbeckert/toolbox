function matrix = replace_nan(matrix, value)
%% replace all NaN's in a matrix with a specified value.
%% Useful for "cellfun"
    matrix(isnan(matrix)) = value;
end