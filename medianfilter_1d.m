function out = medianfilter_1d(data,window)

if size(data,1) < size(data,2)
    data = data';
end

win = round(window/2);

% pad = zeros(win,1);
% data_ = [pad; data; pad];
data_ = data;

for i = win+1:length(data_)-win
    data_(i) = median(data_(i-win:i+win));
end

out = data_(win+1:end-win);

end