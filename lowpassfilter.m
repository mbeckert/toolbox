function [out] = lowpassfilter(in, freq_cutoff, freq_sample, pole)

cutoff = freq_cutoff;

[b,a] = besself(pole, cutoff, 'lowpass');
[bz, az] = impinvar(b, a, freq_sample);

out = filter(bz, az, in);

end