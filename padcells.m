function [output] = padcells(input)

output = input;
m = max(max(cellfun(@length, input)));

for r = 1:size(input,1)
    for c = 1:size(input,2)
        if size(input{r,c},2) > size(input{r,c},1)
            input{r,c} = input{r,c}';
        end
        output{r,c} = [input{r,c}; nan(m - size(input{r,c},1),1)];
    end
end