function [x] = normalization(x,check,dim)

if nargin < 2
    check = 1;
end
if nargin < 3
    dim = 1;
end

x(x < 0) = nan;
if check == 1
    if dim == 2
        x = x';
    end
    for i = 1:size(x,1)
        x(i,:) = x(i,:)./max(x(i,:));
%         x(i,:) = norm01(x(i,:));
%         x(i,:) = nanzscore(x(i,:));
    end
    if dim == 2
        x = x';
    end
end

end