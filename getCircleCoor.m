function [x, y] = getCircleCoor(xCenter, yCenter, sample, radius)

theta = 0 : sample : 2*pi;
x = radius * cos(theta) + xCenter;
y = radius * sin(theta) + yCenter;

end