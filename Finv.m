function r = Finv(z)

r = (exp(2 .* z) - 1) ./ (exp(2 .* z) + 1);

end